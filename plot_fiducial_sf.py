# This script plots the results of the fiducial case with star
# formation included

import numpy as np
import matplotlib.pyplot as plt
from astropy.io.ascii import read as asciiread
from scipy.interpolate import interp1d
from scipy.interpolate import Akima1DInterpolator as akima
import matplotlib.lines as mlines
_path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'
sys.path.append(_path_to_vader)
import vader
sys.path.pop()

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5
kpc = 1e3*pc

# Weighting function to produce Ha-weighted SFR; this comes from a
# SLUG calculation
iontime = np.arange(0.0, 1.01e7, 5e4)*yr
ionwgt = np.array([  4.89568204e+52,   4.88873766e+52,   4.87798438e+52,
         4.86960716e+52,   4.86980737e+52,   4.87490709e+52,
         4.87614391e+52,   4.87999360e+52,   4.88849127e+52,
         4.89478051e+52,   4.90164824e+52,   4.91077249e+52,
         4.91692780e+52,   4.92284661e+52,   4.92865516e+52,
         4.93859988e+52,   4.95323310e+52,   4.96320403e+52,
         4.96745452e+52,   4.97320339e+52,   4.96689784e+52,
         4.73086198e+52,   4.75679906e+52,   4.78107884e+52,
         4.81752225e+52,   4.85695713e+52,   4.89438570e+52,
         4.90326359e+52,   4.88657960e+52,   4.85734634e+52,
         4.82414700e+52,   4.78369250e+52,   4.63979350e+52,
         4.62897761e+52,   4.63223505e+52,   4.63641656e+52,
         4.64192573e+52,   4.52711564e+52,   4.47037002e+52,
         4.42654634e+52,   4.38677549e+52,   4.34614200e+52,
         4.12846167e+52,   3.97776208e+52,   3.68276506e+52,
         3.47406739e+52,   3.33109025e+52,   3.17515020e+52,
         3.05952712e+52,   2.94352096e+52,   2.84890587e+52,
         2.79010657e+52,   2.72588781e+52,   2.56044425e+52,
         2.39711594e+52,   2.31895339e+52,   2.36422184e+52,
         2.26505625e+52,   2.35659105e+52,   2.43937400e+52,
         2.52940996e+52,   2.48191849e+52,   2.37232311e+52,
         2.24141457e+52,   2.14169194e+52,   2.06409288e+52,
         1.98855113e+52,   1.92276808e+52,   1.86027210e+52,
         1.83499756e+52,   1.78462559e+52,   1.73354639e+52,
         1.67378734e+52,   1.60323914e+52,   1.53399455e+52,
         1.43604747e+52,   1.36026770e+52,   1.30364001e+52,
         1.23017111e+52,   1.15256636e+52,   1.11160834e+52,
         1.11089360e+52,   1.09689094e+52,   1.07016366e+52,
         1.04639486e+52,   1.02200808e+52,   9.90475949e+51,
         9.53295872e+51,   9.08822745e+51,   8.65588830e+51,
         8.13025298e+51,   7.70697266e+51,   7.33301977e+51,
         6.74825348e+51,   6.25317019e+51,   5.63436184e+51,
         5.36269919e+51,   5.08849338e+51,   4.85059722e+51,
         4.48350492e+51,   4.27386508e+51,   4.11120362e+51,
         3.95383390e+51,   3.79576210e+51,   3.64776573e+51,
         3.50107517e+51,   3.35061114e+51,   3.18561627e+51,
         3.02072581e+51,   2.87182862e+51,   2.71679589e+51,
         2.55673774e+51,   2.36635909e+51,   2.19569923e+51,
         2.03845694e+51,   1.92652375e+51,   1.84093047e+51,
         1.76350535e+51,   1.68615704e+51,   1.61004084e+51,
         1.53479753e+51,   1.45953164e+51,   1.38368726e+51,
         1.30604253e+51,   1.24014461e+51,   1.19612966e+51,
         1.14937688e+51,   1.09942038e+51,   1.04430004e+51,
         9.88012695e+50,   9.46851493e+50,   9.06130737e+50,
         8.66741302e+50,   8.28685860e+50,   7.92343274e+50,
         7.57590295e+50,   7.24724318e+50,   6.93897687e+50,
         6.65853276e+50,   6.39382824e+50,   6.13706323e+50,
         5.89043300e+50,   5.65443619e+50,   5.42893103e+50,
         5.21468453e+50,   5.00851730e+50,   4.81133374e+50,
         4.62259397e+50,   4.44247687e+50,   4.27154313e+50,
         4.10826286e+50,   3.95116112e+50,   3.80150092e+50,
         3.65925718e+50,   3.52812300e+50,   3.40457932e+50,
         3.28639007e+50,   3.16477913e+50,   3.06151102e+50,
         2.96552585e+50,   2.87223628e+50,   2.78162691e+50,
         2.69361465e+50,   2.60916772e+50,   2.52676321e+50,
         2.44754619e+50,   2.37141833e+50,   2.29859296e+50,
         2.22762233e+50,   2.15928086e+50,   2.09351547e+50,
         2.03015281e+50,   1.96973486e+50,   1.91239803e+50,
         1.85965584e+50,   1.81012327e+50,   1.76136886e+50,
         1.71399562e+50,   1.66771503e+50,   1.62245296e+50,
         1.57859833e+50,   1.53561956e+50,   1.49340421e+50,
         1.45298019e+50,   1.41337929e+50,   1.37523193e+50,
         1.33792189e+50,   1.30190638e+50,   1.26730160e+50,
         1.23373189e+50,   1.20085739e+50,   1.16895667e+50,
         1.13694970e+50,   1.10615153e+50,   1.07649352e+50,
         1.04750389e+50,   1.02050303e+50,   9.94781456e+49,
         9.71072109e+49,   9.47645370e+49,   9.25214904e+49,
         9.03123017e+49])

# Read parameters
paramFile = 'cmzdisk.param'
paramDict = vader.readParam(paramFile)

# Get mass versus r and build vphi vs. r
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Read the simulation results
data = np.load('fiducial_sf_epsff005_eta0.npz')
tOut = data['tOut']
col = data['col']
pres = data['pres']
mBnd = data['mBnd']
mSrc = data['mSrc']
etaML = data['etaML']

# Make ion weighting
iontimeinterp = np.arange(0,1.0e7*yr,tOut[1]-tOut[0])
ionwgtinterp = interp1d(iontime, ionwgt)(iontimeinterp)
ionwgtinterp = ionwgtinterp / np.sum(ionwgtinterp)

# Compute SFR per unit area
colsfr = np.zeros(col.shape)
colsfr[0,:] = (mSrc[0,:]-mSrc[1,:])/(tOut[1]-tOut[0])/(1.0+etaML)
colsfr[-1,:] = (mSrc[-2,:]-mSrc[-1,:])/(tOut[-1]-tOut[-2])/(1.0+etaML)
colsfr[1:-1,:] = np.transpose(np.transpose(
    mSrc[2:,:]-mSrc[:-2,:])/(tOut[:-2]-tOut[2:]))/(1.0+etaML)

# 250 pc ring
# 10 pc diameter ring
# 22.5 Myr

# Compute gas surface density and SFR per unit area within 250 pc
sigmagaskpc = np.zeros(tOut.shape)
sigmasfrkpc = np.zeros(tOut.shape)
mgaskpc = np.zeros(tOut.shape)
sfrkpc = np.zeros(tOut.shape)
for i in range(len(tOut)):
    idxra = np.where(grd.r <= 250*pc)[0]
    mgaskpc[i] \
        = np.pi* np.sum(col[i, idxra]*
                        (grd.r_h[idxra+1]**2-grd.r_h[idxra]**2)) 
    sfrkpc[i] \
        = np.pi * np.sum(colsfr[i, idxra]*
                         (grd.r_h[idxra+1]**2-grd.r_h[idxra]**2))
    areakpc = np.pi * (grd.r_h[idxra[-1]+1]**2 - grd.r_h[idxra[0]]**2)
    sigmagaskpc[i] = mgaskpc[i] / areakpc
    sigmasfrkpc[i] = sfrkpc[i] / areakpc
tdep10pc = sigmagaskpc/sigmasfrkpc


# Compute gas surface density and SFR per unit area in an annulus from
# 80 - 90 pc
sigmagas10pc = np.zeros(tOut.shape)
sigmasfr10pc = np.zeros(tOut.shape)
mgas10pc = np.zeros(tOut.shape)
sfr10pc = np.zeros(tOut.shape)
for i in range(len(tOut)):
    idxra = np.where(np.abs(grd.r - 85*pc) <= 5*pc)[0]
    mgas10pc[i] \
        = np.pi* np.sum(col[i, idxra]*
                        (grd.r_h[idxra+1]**2-grd.r_h[idxra]**2)) 
    sfr10pc[i] \
        = np.pi * np.sum(colsfr[i, idxra]*
                         (grd.r_h[idxra+1]**2-grd.r_h[idxra]**2)) 
    area10pc = np.pi * (grd.r_h[idxra[-1]+1]**2 - grd.r_h[idxra[0]]**2)
    sigmagas10pc[i] = mgas10pc[i] / area10pc
    sigmasfr10pc[i] = sfr10pc[i] / area10pc
tdep10pc = sigmagas10pc/sigmasfr10pc

# Compute quantities averaged by Ha
sfr1kpcsmooth = np.convolve(sfrkpc, ionwgtinterp[::-1], mode='same')
sigmasfrsmooth1kpc = sfr1kpcsmooth / areakpc
tdep1kpcsmooth = mgaskpc/sfr1kpcsmooth
sfr10pcsmooth = np.convolve(sfr10pc, ionwgtinterp[::-1], mode='same')
sigmasfrsmooth10pc = sfr10pcsmooth / area10pc
tdep10pcsmooth = mgas10pc/sfr10pcsmooth

# Make smoothed Q
vdisp = np.sqrt(pres/col)
omega = grd.vphi/grd.r
t_orb = 2.0*np.pi/omega
kappa = np.sqrt(2*(grd.beta+1))*omega
kcrit = kappa**2 / (2.0*np.pi*G*col)
Q = kappa*vdisp / (np.pi*G*col)
rPad = np.pad(grd.r, (1,1), mode='edge')
rPad[0] = grd.r_h[0]
rPad[-1] = grd.r_h[-1]
colPad = np.pad(col, ((0,0),(1,1)), mode='edge')
colsfrPad = np.pad(colsfr, ((0,0),(1,1)), mode='edge')
QPad = np.pad(Q, ((0,0),(1,1)), mode='edge')
QInterp = akima(np.log(rPad), np.log(np.transpose(QPad)))
colInterp = akima(np.log(rPad), np.log(np.transpose(colPad)))
colsfrInterp = akima(np.log(rPad), np.log(
    np.transpose(colsfrPad+1e-300)))
rHR = np.linspace(grd.r_h[0], grd.r_h[-1], 32768)
QHR = np.exp(np.transpose(
    QInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
colHR = np.exp(np.transpose(
    colInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
colsfrHR = np.exp(np.transpose(
    colsfrInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
colsfrHR[np.isnan(colsfrHR)]=1e-300

# Generate fake tracks with gas expulsion
texpel = 22.5
dtexpel = 4
colsfr_expel = np.copy(colsfrHR)
col_expel = np.copy(colHR)
idx = np.where(np.logical_and(tOut > texpel*Myr,
                              tOut <= (texpel+dtexpel)*Myr))[0]
for i in idx:
    fac1 = 1.0 - 0.9 * (tOut[i]/Myr-texpel) / dtexpel
    fac2 = 1.0 - (tOut[i]/Myr-texpel) / dtexpel
    idx1 = np.where(QHR[i,:] < 3)[0]
    colsfr_expel[i,idx1] = colsfr_expel[i,idx1]*fac2
    col_expel[i,idx1] = col_expel[i,idx1]*fac1
idx = np.where(tOut > (texpel+dtexpel)*Myr)[0]
for i in idx:
    idx1 = np.where(QHR[i,:] < 3)[0]
    colsfr_expel[i,idx1] = 0.0*colsfr_expel[i,idx1]
    col_expel[i,idx1] = 0.1*col_expel[i,idx1]

# Repeat area averaging on smoothed quantities
sigmagas1kpc_expel = np.zeros(tOut.shape)
sigmasfr1kpc_expel = np.zeros(tOut.shape)
mgaskpc_expel = np.zeros(tOut.shape)
sfrkpc_expel = np.zeros(tOut.shape)
sigmagas10pc_expel = np.zeros(tOut.shape)
sigmasfr10pc_expel = np.zeros(tOut.shape)
mgas10pc_expel = np.zeros(tOut.shape)
sfr10pc_expel = np.zeros(tOut.shape)
for i in range(len(tOut)):
    idxra = np.where(rHR < 250*pc)[0]
    if idxra[-1] == len(rHR)-1:
        idxra = idxra[:-1]
    mgaskpc_expel[i] \
        = np.pi* np.sum(col_expel[i, idxra]*
                        (rHR[idxra+1]**2-rHR[idxra]**2)) 
    sfrkpc_expel[i] \
        = np.pi * np.sum(colsfr_expel[i, idxra]*
                         (rHR[idxra+1]**2-rHR[idxra]**2)) 
    sigmagas1kpc_expel[i] = mgaskpc_expel[i] / areakpc
    sigmasfr1kpc_expel[i] = sfrkpc_expel[i] / areakpc
    idxra = np.where(np.abs(rHR - 85*pc) <= 5*pc)[0]
    if idxra[-1] == len(rHR)-1:
        idxra = idxra[:-1]
    mgas10pc_expel[i] \
        = np.pi* np.sum(col_expel[i, idxra]*
                        (rHR[idxra+1]**2-rHR[idxra]**2)) 
    sfr10pc_expel[i] \
        = np.pi * np.sum(colsfr_expel[i, idxra]*
                         (rHR[idxra+1]**2-rHR[idxra]**2)) 
    sigmagas10pc_expel[i] = mgas10pc_expel[i] / area10pc
    sigmasfr10pc_expel[i] = sfr10pc_expel[i] / area10pc
tdep10pc_expel = sigmagas10pc_expel/sigmasfr10pc_expel
sfr1kpcsmooth_expel = np.convolve(sfrkpc_expel, ionwgtinterp[::-1], mode='same')
sigmasfrsmooth1kpc_expel = sfr1kpcsmooth_expel / areakpc
tdep1kpcsmooth_expel = mgaskpc_expel/sfr1kpcsmooth_expel
sfr10pcsmooth_expel = np.convolve(sfr10pc_expel, ionwgtinterp[::-1],
                                  mode='same')
sigmasfrsmooth10pc_expel = sfr10pcsmooth_expel / area10pc
tdep10pcsmooth_expel = mgas10pc_expel/sfr10pcsmooth_expel


# Make plots
plt.figure(1, figsize=(6,8))
plt.clf()

# SFR
ax = plt.subplot(3,1,1)
plt.plot(tOut/Myr, sfrkpc/(Msun/yr), 'b--', label='Instantaneous')
plt.plot(tOut/Myr, sfr1kpcsmooth/(Msun/yr), 'b--', lw=3, label='Observed')
plt.plot(tOut/Myr, sfrkpc_expel/(Msun/yr), 'b', label='Expulsion, inst.')
plt.plot(tOut/Myr, sfr1kpcsmooth_expel/(Msun/yr), 'b', lw=3, label='Expulsion, obs.')
plt.ylabel(r'SFR [$M_\odot$ yr$^{-1}$]')
plt.setp(ax.get_xticklabels(), visible=False)
plt.legend(loc='upper left', prop={'size':12})
plt.xlim([0,30])
plt.ylim([0,0.4])

# Gas mass
ax = plt.subplot(3,1,2)
plt.plot(tOut/Myr, mgaskpc/(1e6*Msun), 'b--', label=r'500 pc')
plt.plot(tOut/Myr, mgas10pc/(1e6*Msun), 'g--', label=r'10 pc')
plt.plot(tOut/Myr, mgaskpc_expel/(1e6*Msun), 'b', label=r'500 pc, exp.')
plt.plot(tOut/Myr, mgas10pc_expel/(1e6*Msun), 'g', label=r'10 pc, exp.')
plt.ylabel(r'$M_{\mathrm{gas}}$ [$10^6$ $M_\odot$]')
plt.setp(ax.get_xticklabels(), visible=False)
plt.legend(loc='upper left', prop={'size':12})
plt.xlim([0,30])
plt.ylim([0,24])

# Depletion time
ax = plt.subplot(3,1,3)
p1,=plt.plot(tOut/Myr, tdep1kpcsmooth/Gyr, 'b--', lw=3)
lab1=r'500 pc, obs.'
p2,=plt.plot(tOut/Myr, tdep10pcsmooth/Gyr, 'g--', lw=3)
lab2=r'10 pc, obs.'
p3,=plt.plot(tOut/Myr, tdep1kpcsmooth_expel/Gyr, 'b', lw=3) 
lab3=r'500 pc, exp.'
p4,=plt.plot(tOut/Myr, tdep10pcsmooth_expel/Gyr, 'g', lw=3)
lab4=r'10 pc, exp.'
plt.yscale('log')
plt.ylim([1e-2,3e1])
plt.ylabel(r'$t_{\mathrm{dep}}$ [Gyr]')
plt.xlabel('$t$ [Myr]')
plt.xlim([0,30])
leg1=plt.legend([p1,p2,p3,p4], [lab1, lab2,lab3,lab4],
                loc='lower left', prop={'size':12})

# Adjust spacing
plt.subplots_adjust(hspace=0, bottom=0.07, top=0.95, right=0.95, left=0.15)

# Save
plt.savefig('fiducial_sf.pdf')

# Make KS plot
plt.figure(2, figsize=(10,5))
plt.clf()

# Sigma_SFR vs. Sigma plot
ax = plt.subplot(1,2,1)

# Accumulation phase
t0 = 14
tPresent = 17.5
tstart = t0
tend = texpel
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
tidx = np.append(tidx, [tidx[-1]+1])
plt.plot(sigmagas1kpc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2)*1.3,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'0 Myr, start',
         verticalalignment='bottom',
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2)*1.3,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'0 Myr, start',
         verticalalignment='bottom',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
idx = int(0.75*tidx[0]+0.25*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/(Msun/pc**2)*1.1,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)/1.1,
         'Accumulation',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Burst / expulsion phase
tstart = texpel
tend = texpel+dtexpel
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
tidx = np.append(tidx, [tidx[-1]+1])
plt.plot(sigmagas1kpc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2)*1.3,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2)*1.3,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
#plt.text(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2)*1.3,
#         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
#         r'{:4.1f} Myr'.format(tstart-t0),
#         verticalalignment='bottom',
#         fontdict = { 'fontsize' : 10,
#                      'color' : 'b' })
#plt.text(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2)*1.3,
#         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
#         r'{:4.1f} Myr'.format(tstart-t0),
#         verticalalignment='bottom',
#         fontdict = { 'fontsize' : 10,
#                      'color' : 'g' })
idx = int(0.25*tidx[0]+0.75*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)*1.3,
         'Expulsion', horizontalalignment='center',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Fading phase
tstart = texpel+dtexpel
tend = texpel+dtexpel+4.99
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
plt.plot(sigmagas1kpc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2)/1.3,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2)/1.3,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:4.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
idx = int(0.2*tidx[0]+0.8*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/(Msun/pc**2)/1.2,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2),
         'Fading', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Recovery phase
tidx = [tidx[-1], np.argmin(np.abs(tOut - t0*Myr))]
plt.plot(sigmagas1kpc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b--', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g--', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)

# Time average
tstart = t0
tend = texpel+dtexpel+4.99
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
sigmagas1kpc_avg = np.mean(sigmagas1kpc_expel[tidx])
sigmasfrsmooth1kpc_expel_avg = np.mean(sigmasfrsmooth1kpc_expel[tidx])
sigmagas10pc_avg = np.mean(sigmagas10pc_expel[tidx])
sigmasfrsmooth10pc_expel_avg = np.mean(sigmasfrsmooth10pc_expel[tidx])
plt.plot([sigmagas1kpc_avg/(Msun/pc**2)],
         [sigmasfrsmooth1kpc_expel_avg/(Msun/pc**2/Myr)],
         'bD', ms=10)
avg1=[sigmagas1kpc_avg/(Msun/pc**2),
      sigmasfrsmooth1kpc_expel_avg/(Msun/pc**2/Myr)]
plt.plot([sigmagas10pc_avg/(Msun/pc**2)],
         [sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)],
         'gD', ms=10)
avg2=[sigmagas10pc_avg/(Msun/pc**2),
      sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)]
plt.text(sigmagas10pc_avg/(Msun/pc**2)*1.25,
         sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)/1.5,
         'Average',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'g' })

# Present-day CMZ
idx = np.argmin(np.abs(tOut/Myr-tPresent))
plt.plot(sigmagas1kpc_expel[idx]/(Msun/pc**2),
         sigmasfrsmooth1kpc_expel[idx]/(Msun/yr/kpc**2),
         'b*', ms=15)
plt.text(sigmagas1kpc_expel[idx]/(Msun/pc**2)*1.3,
         sigmasfrsmooth1kpc_expel[idx]/(Msun/yr/kpc**2)/1.2,
         '"CMZ"',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'b' })
plt.plot(sigmagas10pc_expel[idx]/(Msun/pc**2),
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2),
         'g*', ms=15)
plt.text(sigmagas10pc_expel[idx]/(Msun/pc**2)*1.3,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)/1.1,
         '"CMZ"',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'g' })

# Comparisons
xvec=np.logspace(0,5,100)
plt.plot(xvec, xvec/2e3, 'r--')
plt.plot(xvec, 1.6*xvec**1.4*2.5e-4, 'k:')
plt.plot(xvec, xvec**1.42 * 10.**-3.83, 'm-.')
plt.plot(xvec, xvec**1.42 * 10.**(-3.83+0.9), 'm-.')

# Adjust axes
plt.xscale('log')
plt.yscale('log')
plt.xlim([2,9e3])
yra = [5e-5,1e2]
plt.ylim(yra)

# Labels
plt.xlabel(r'$\Sigma$ [$M_\odot$ pc$^{-2}$]')
plt.ylabel(r'$\Sigma_{\mathrm{SFR}}$ [$M_\odot$ pc$^{-2}$ Myr$^{-1}$]')

# Sigma_SFR vs. Sigma/t_orb plot
ax = plt.subplot(1,2,2)
torb1kpc = 2*np.pi*grd.r[-1]/grd.vphi[-1]
idx = np.argmin(np.abs(grd.r/pc-90))
torb10pc = 2*np.pi*grd.r[idx]/grd.vphi[idx]

# Accumulation phase
tstart = t0
tend = texpel
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
tidx = np.append(tidx, [tidx[-1]+1])
plt.plot(sigmagas1kpc_expel[tidx]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr)*1.7,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:d} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr)*1.8,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
idx = int(0.75*tidx[0]+0.25*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/torb10pc/(Msun/pc**2/Myr)*1.6,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)/1.1,
         'Accumulation',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Burst / expulsion phase
tstart = texpel
tend = texpel+dtexpel
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
tidx = np.append(tidx, [tidx[-1]+1])
plt.plot(sigmagas1kpc_expel[tidx]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr)*1.7,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         bbox = { 'facecolor' : 'w', 'edgecolor' : 'w', 'alpha' :
                  0.75 }, 
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr)*1.8,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:3.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom',
         bbox = { 'facecolor' : 'w', 'edgecolor' : 'w', 'alpha' :
                  0.75 }, 
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
idx = int(0.3*tidx[0]+0.7*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)*1.3,
         'Expulsion', horizontalalignment='center',
         bbox = { 'facecolor' : 'w', 'edgecolor' : 'w', 'alpha' :
                  0.75 }, 
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Fading phase
tstart = texpel+dtexpel
tend = texpel+dtexpel+4.99
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
plt.plot(sigmagas1kpc_expel[tidx]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)
plt.text(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr)/1.8,
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:4.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas1kpc_expel[tidx[-1]]/torb1kpc/(Msun/pc**2/Myr)/1.6,
         sigmasfrsmooth1kpc_expel[tidx[-1]]/(Msun/yr/kpc**2)/1.1,
         r'{:4.1f} Myr'.format(int(round(tend-t0))),
         verticalalignment='bottom', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'b' })
plt.text(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr)/1.6,
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2)/1.1,
         r'{:4.1f} Myr'.format(tstart-t0),
         verticalalignment='bottom', horizontalalignment='right',
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })
#plt.text(sigmagas10pc_expel[tidx[-1]]/torb10pc/(Msun/pc**2/Myr)/1.6,
#         sigmasfrsmooth10pc_expel[tidx[-1]]/(Msun/yr/kpc**2)/1.1,
#         r'{:4.1f} Myr'.format(int(round(tend-t0))),
#         verticalalignment='bottom', horizontalalignment='right',
#         fontdict = { 'fontsize' : 10,
#                      'color' : 'g' })
idx = int(0.2*tidx[0]+0.8*tidx[-1])
plt.text(sigmagas10pc_expel[idx]/torb10pc/(Msun/pc**2/Myr)/1.5,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2),
         'Fading', horizontalalignment='right',
         bbox = { 'facecolor' : 'w', 'edgecolor' : 'w', 'alpha' :
                  0.75 }, 
         fontdict = { 'fontsize' : 10,
                      'color' : 'g' })

# Recovery phase
tidx = [tidx[-1], np.argmin(np.abs(tOut - t0*Myr))]
plt.plot(sigmagas1kpc_expel[tidx]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx]/(Msun/yr/kpc**2),
         'b--', lw=2)
plt.plot(sigmagas10pc_expel[tidx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx]/(Msun/yr/kpc**2),
         'g--', lw=2)
plt.plot(sigmagas1kpc_expel[tidx[0]]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'bo', ms=10)
plt.plot(sigmagas10pc_expel[tidx[0]]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[tidx[0]]/(Msun/yr/kpc**2),
         'go', ms=10)

# Present-day CMZ
idx = np.argmin(np.abs(tOut/Myr-tPresent))
plt.plot(sigmagas1kpc_expel[idx]/torb1kpc/(Msun/pc**2/Myr),
         sigmasfrsmooth1kpc_expel[idx]/(Msun/yr/kpc**2),
         'b*', ms=15)
plt.text(sigmagas1kpc_expel[idx]/torb1kpc/(Msun/pc**2/Myr)*1.6,
         sigmasfrsmooth1kpc_expel[idx]/(Msun/yr/kpc**2)/1.2,
         '"CMZ"',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'b' })
plt.plot(sigmagas10pc_expel[idx]/torb10pc/(Msun/pc**2/Myr),
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2),
         'g*', ms=15)
plt.text(sigmagas10pc_expel[idx]/torb10pc/(Msun/pc**2/Myr)*1.6,
         sigmasfrsmooth10pc_expel[idx]/(Msun/yr/kpc**2)/1.2,
         '"CMZ"',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'g' })

# Time average
tstart = t0
tend = texpel+dtexpel+4.99
tidx = np.where(np.logical_and(tOut >= tstart*Myr, 
                               tOut <= tend*Myr))[0]
sigmagas1kpc_avg = np.mean(sigmagas1kpc_expel[tidx])
sigmasfrsmooth1kpc_expel_avg = np.mean(sigmasfrsmooth1kpc_expel[tidx])
sigmagas10pc_avg = np.mean(sigmagas10pc_expel[tidx])
sigmasfrsmooth10pc_expel_avg = np.mean(sigmasfrsmooth10pc_expel[tidx])
plt.plot([sigmagas1kpc_avg/torb1kpc/(Msun/pc**2/Myr)],
         [sigmasfrsmooth1kpc_expel_avg/(Msun/pc**2/Myr)],
         'bD', ms=10)
avg1=[sigmagas1kpc_avg/torb1kpc/(Msun/pc**2/Myr),
      sigmasfrsmooth1kpc_expel_avg/(Msun/pc**2/Myr)]
plt.plot([sigmagas10pc_avg/torb10pc/(Msun/pc**2/Myr)],
         [sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)],
         'gD', ms=10)
avg2=[sigmagas10pc_avg/torb10pc/(Msun/pc**2/Myr),
      sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)]
plt.text(sigmagas10pc_avg/torb10pc/(Msun/pc**2/Myr)*1.6,
         sigmasfrsmooth10pc_expel_avg/(Msun/pc**2/Myr)/1.5,
         'Average',
         bbox = { 'facecolor' : 'w',
                  'edgecolor' : 'w',
                  'alpha' : 0.75 },
         fontdict = { 'fontsize' : 10, 'color' : 'g' })

# Comparisons
xvec=np.logspace(-2,5,100)
plt.plot(xvec, 1.6*0.017*2.0*np.pi*xvec, 'k:')
plt.plot(xvec, 10.**-0.62*xvec**1.14, 'm-.')

# Labels
plt.setp(ax.get_yticklabels(), visible=False)
plt.xlabel(r'$\Sigma/t_{\mathrm{orb}}$ [$M_\odot$ pc$^{-2}$ Myr$^{-1}$]')

# Legend
kpc_line = mlines.Line2D([], [], color='b', lw=2, label='500 pc resolution')
pc_line = mlines.Line2D([], [], color='g', lw=2, label='10 pc resolution')
k98_line = mlines.Line2D([], [], color='k', linestyle=':',
                         label= 'Kennicutt (1998)')
b08_line = mlines.Line2D([], [], color='r', linestyle='--',
                         label = 'Bigiel+ (2008)')
d10_line = mlines.Line2D([], [], color='m', linestyle='-.',
                         label='Daddi+ (2010)')
plt.legend(handles=[kpc_line, pc_line, k98_line, b08_line,
                    d10_line],
           loc='lower right',
           prop = { 'size' : 12 })


# Adjust spacing
plt.xscale('log')
plt.yscale('log')
plt.xlim([1e-2,5e4])
plt.ylim(yra)
plt.subplots_adjust(wspace=0, right=0.95, top=0.95, left=0.1, bottom=0.12)

# Save
plt.savefig('fiducial_ks.pdf')
