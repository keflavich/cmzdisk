# Parameter file for CMZ disk simulations

# General parameters
nr		    = 512
rmin		    = 3.09e19
rmax		    = 1.39e21
alpha		    = c_func
rot_curve_type	    = tabulated
ibc_pres_type	    = fixed_torque_flux
ibc_pres_val	    = 0.0
ibc_enth_type	    = fixed_value
obc_pres_type	    = fixed_mass_flux
obc_pres_val	    = -6.3e25           # 1 Msun/yr
obc_enth_type	    = fixed_value
obc_enth_val	    = 4.0e13            # 2.5 * (40 km/s)**2
gamma		    = 1.66667
int_en_src	    = c_func
verbosity	    = 1
bspline_degree	    = 6
bspline_breakpoints = 15
max_step	    = 500000
dt_tol		    = 0.5
method		    = BE

# Problem specific parameters
eta	  	    = 1.5               # Dimensionless dissipation rate
init_col	    = 1.0		# Initial column density, in Msun/pc^2
init_vdisp	    = 40.0		# Initial velocity dispersion, in km/s
sigmath		    = 0.5		# Thermal velocity dispersion, in km/s
shapefac	    = 2.5		# Stellar density normalized to spherical case
zetad		    = 0.33		# OML10 shape parameter
m_vs_r_file	    = launhardt02.dat   # File containing enclosed mass vs. r
alphacoef	    = 1.0		# Coefficient on alpha