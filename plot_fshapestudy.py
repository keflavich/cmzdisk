# This script plots the results of the runs with varying Mdot

# Import libraries
import numpy as np
import matplotlib.pyplot as plt
import numpy.polynomial.polynomial as poly
import ctypes
import sys
import os
from scipy.interpolate import Akima1DInterpolator as akima
from astropy.io.ascii import read as asciiread
_path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'
sys.path.append(_path_to_vader)
import vader
sys.path.pop()

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
kmps = 1e5

# Times to plot
tplot = np.array([0, 10, 25, 50])*Myr

# Read parameters
paramFile = 'cmzdisk.param'
paramDict = vader.readParam(paramFile)

# Get mass versus r and build vphi vs. r
mvsr = asciiread(paramDict['m_vs_r_file'])
ndata = len(mvsr['col1'].data)
rotcurvetab = np.zeros((2, ndata))
rotcurvetab[0,:]=mvsr['col1'].data*pc
rotcurvetab[1,:]=np.sqrt(G*mvsr['col2'].data*Msun/rotcurvetab[0,:])

# Construct grid
grd = vader.grid(paramDict, rotCurveTab=rotcurvetab)

# Read the fiducial simulation results and prepare grid to hold data
data = np.load('fiducial.npz')
tOut = data['tOut']
col = np.zeros((3,)+data['col'].shape)
pres = np.zeros((3,)+data['pres'].shape)
mBnd = np.zeros((3,)+data['mBnd'].shape)

# Read data
col[1,:,:] = data['col']
pres[1,:,:] = data['pres']
mBnd[1,:,:] = data['mBnd']

# Read the varying alpha cases
data = np.load('fshape1.npz')
col[0,:,:] = data['col']
pres[0,:,:] = data['pres']
mBnd[0,:,:] = data['mBnd']
data = np.load('fshape5.npz')
col[2,:,:] = data['col']
pres[2,:,:] = data['pres']
mBnd[2,:,:] = data['mBnd']

# Compute Q
vdisp = np.sqrt(pres/col)
omega = grd.vphi/grd.r
t_orb = 2.0*np.pi/omega
kappa = np.sqrt(2*(grd.beta+1))*omega
Q = kappa*vdisp / (np.pi*G*col+1e-30)
nu = np.sqrt(1.0 - 1.0/Q**2 + 0j)
tgrowth_GI = 1.0 / (kappa*np.imag(nu)+1e-50) / t_orb

# Growth time for acoustic instabilities
m = 2
T1 = -(2.0*m*omega/(kappa*grd.r))**2*(grd.beta-1)
kcrit = kappa**2 / (2.0*np.pi*G*col)
J = np.sqrt(T1)/kcrit
tgrowth_ac = np.zeros(col.shape)
coef = np.zeros(6)

#for k in range(col.shape[0]):
for k in range(0):
    for i in range(col.shape[1]):
        for j in range(col.shape[2]):
        
            # Coefficients in polynomial for wavenumber at most unstable mode
            coef[0] = Q[k,i,j]**4
            coef[1] = -6*Q[k,i,j]**2
            coef[2] = 8
            coef[5] = -16*J[k,i,j]**2

            # Solve for most unstable wavenumber; discard non-real and
            # negative real roots
            roots = poly.polyroots(coef)
            eta = roots[
                np.logical_and(np.real(roots)>0, 
                               np.abs(np.imag(roots))<1.e-15)]

            # Plug candidates for most unstable wavenumber into
            # dispersion relation get dimensionless frequency
            rootpart = np.sqrt((Q[k,i,j]**2-4*eta)*
                               (Q[k,i,j]**2-4*eta-16*J[k,i,j]**2*eta**4))
            rootpart = np.array([rootpart, -rootpart])/(8*eta**2)
            otherpart = (Q[k,i,j]**2-4*eta+8*eta**2) / (8*eta**2)
            nu = np.sqrt(otherpart + rootpart)

            # Get maximum imaginary part; this gives fastest
            # growing mode
            nufast = np.amax(np.imag(nu))

            # Convert to growth time measured in orbital periods
            tgrowth_ac[k,i,j] = 1.0 / (nufast*kappa[j]) / t_orb[j]

# Compute alpha and mdot
alpha = np.exp(1.0 - np.minimum(tgrowth_GI, tgrowth_ac))
alpha[alpha < 1.0e-3] = 1.0e-3
alpha[alpha > 1.0] = 1.0
mDotIn = np.zeros((3,len(tOut),grd.nr+1))
mDotIn[:,:,1:-1] = grd.g_h[1:-1] * \
                 ((1.0-grd.beta[1:]) * grd.r[1:]**2 * 
                  alpha[:,:,1:] * pres[:,:,1:] - 
                  (1.0-grd.beta[:-1]) * grd.r[:-1]**2 * 
                  alpha[:,:,:-1] * pres[:,:,:-1])
mDotIn[:,:,0] = -mBnd[:,:,0]/(tOut+1.0e-50)
mDotIn[:,:,-1] = -mBnd[:,:,1]/(tOut+1.0e-50)
mDotIn[:,0,-1] = -paramDict['obc_pres_val']

# Mass and mass-weighted mean radius in Q < 1 region and Q < 10
# region; use interpolation to make this behave smoothly
rPad = np.pad(grd.r, (1,1), mode='edge')
rPad[0] = grd.r_h[0]
rPad[-1] = grd.r_h[-1]
nHR = 32768
rHR = np.linspace(grd.r_h[0], grd.r_h[-1], nHR)
QHR = np.zeros(Q.shape[:2]+(nHR-1,))
colHR = np.zeros(Q.shape[:2]+(nHR-1,))
for i in range(Q.shape[0]):
    colPad = np.pad(col[i,:,:], ((0,0),(1,1)), mode='edge')
    QPad = np.pad(Q[i,:,:], ((0,0),(1,1)), mode='edge')
    QInterp = akima(np.log(rPad), np.log(np.transpose(QPad)))
    colInterp = akima(np.log(rPad), np.log(np.transpose(colPad)))
    QHR[i,:,:] = np.exp(np.transpose(
        QInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
    colHR[i,:,:] = np.exp(np.transpose(
        colInterp(0.5*(np.log(rHR[1:])+np.log(rHR[:-1])))))
mUnstable = np.sum(colHR * (QHR<1.01) * np.pi *
                   (rHR[1:]**2-rHR[:-1]**2), axis=2)
rUnstable = np.sum(np.sqrt(rHR[1:]*rHR[:-1]) * colHR * (QHR<1.01) * np.pi *
                   (rHR[1:]**2-rHR[:-1]**2), axis=2) / (mUnstable + 1.0e-30)
mQ10 = np.sum(colHR * (QHR<10) * np.pi *
              (rHR[1:]**2-rHR[:-1]**2), axis=2)
rQ10 = np.sum(np.sqrt(rHR[1:]*rHR[:-1]) * colHR * (QHR<10) * np.pi *
              (rHR[1:]**2-rHR[:-1]**2), axis=2) / (mQ10 + 1.0e-30)

# Make comparison plots
idx = [150, 450]
labels = [r'$f_{\mathrm{shape}}=1$',
          r'$f_{\mathrm{shape}}=2.5$',
          r'$f_{\mathrm{shape}}=5$']
rra = [paramDict['rmin']/pc, paramDict['rmax']/pc]

plt.figure(1, figsize=(8,6))
plt.clf()

# Column density, first time
lw = [1, 3, 3]
ls = ['-', '-', '--']
lc = ['b', 'k', 'g']
ax=plt.subplot(2,2,1)
for i in range(3):
    plt.plot(grd.r/pc, col[i,idx[0],:]/(Msun/pc**2), lc[i]+ls[i], lw=lw[i],
             label=labels[i])
plt.xlim(rra)
plt.ylim([0.5,2e4])
plt.yscale('log')
plt.ylabel(r'$\Sigma$ [$M_\odot/\mathrm{pc}^2$]')
plt.setp(ax.get_xticklabels(), visible=False)
plt.title(r'$t = {:2d}$ Myr'.format(int(np.round(tOut[idx[0]]/Myr))))
plt.legend(loc='upper right')

# Column density, second time
ax=plt.subplot(2,2,2)
for i in range(3):
    plt.plot(grd.r/pc, col[i,idx[1],:]/(Msun/pc**2), lc[i]+ls[i], lw=lw[i],
             label=labels[i])
plt.xlim(rra)
plt.ylim([0.5,2e4])
plt.yscale('log')
plt.setp(ax.get_xticklabels(), visible=False)
plt.setp(ax.get_yticklabels(), visible=False)
plt.title(r'$t = {:2d}$ Myr'.format(int(np.round(tOut[idx[1]]/Myr))))

# Q, first time
ax=plt.subplot(2,2,3)
for i in range(3):
    plt.plot(grd.r/pc, Q[i,idx[0],:], lc[i]+ls[i], lw=lw[i],
             label=labels[i])
plt.yscale('log')
plt.xlabel('r [pc]')
plt.ylabel(r'Q')
plt.xlim(rra)
plt.ylim([1e-1,9e3])

# Q, second time
ax=plt.subplot(2,2,4)
for i in range(3):
    plt.plot(grd.r/pc, Q[i,idx[1],:], lc[i]+ls[i], lw=lw[i],
             label=labels[i])
plt.yscale('log')
plt.xlabel('r [pc]')
plt.xlim(rra)
plt.ylim([1e-1,9e3])
plt.setp(ax.get_yticklabels(), visible=False)

# Spacing
plt.subplots_adjust(hspace=0, wspace=0)

# Save
plt.savefig('fshapestudy1.pdf')

# Second figure
plt.figure(2, figsize=(6,6))
plt.clf()
ax=plt.subplot(2,1,1)
for i in range(3):
    plt.plot(tOut/Myr, mUnstable[i,:]/(1e6*Msun), lc[i]+ls[i], lw=lw[i], label=labels[i])
plt.legend(loc='upper left')
plt.setp(ax.get_xticklabels(), visible=False)
plt.ylabel(r'$M$ [$10^6\,M_\odot$]')

ax=plt.subplot(2,1,2)
for i in range(3):
    plt.plot(tOut[mUnstable[i,:]>0]/Myr, 
             rUnstable[i,mUnstable[i,:]>0]/pc,
             lc[i]+ls[i], lw=lw[i], label=labels[i])
plt.ylabel(r'$\langle r\rangle_M$ [pc]')
plt.xlabel(r'$t$ [Myr]')
plt.xlim([0,50])
plt.ylim([0,159])

plt.subplots_adjust(hspace=0)

# Save
plt.savefig('fshapestudy2.pdf')
